package com.grapeup.ratingservice.repository;

import com.grapeup.ratingservice.entity.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * DAO class for rating
 */
@Repository
@Transactional
public interface RatingRepository extends JpaRepository<Rating, Long> {

    @Query("SELECT r FROM Rating r WHERE r.roomId = :roomId")
    List<Rating> findRatingsByRoomId(@Param("roomId") Long roomId);

}
