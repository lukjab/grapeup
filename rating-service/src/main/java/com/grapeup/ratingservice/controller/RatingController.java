package com.grapeup.ratingservice.controller;

import com.grapeup.ratingservice.entity.Rating;
import com.grapeup.ratingservice.repository.RatingRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller class for rating service
 */
@RestController
@RequestMapping(value = "/ratings")
public class RatingController {

    private RatingRepository ratingRepository;
    private String ratingServiceNumber;

    public RatingController(RatingRepository ratingRepository, @Value("${RATING_SERVICE_NUMBER}") String ratingServiceNumber) {
        this.ratingRepository = ratingRepository;
        this.ratingServiceNumber = ratingServiceNumber;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<Rating> getAllRatings() {
        List<Rating> allRatings = ratingRepository.findAll();

        return allRatings.stream()
                .peek(rating -> rating.setRunningServiceNumber(ratingServiceNumber))
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/{roomId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<Rating> getRatingsForRoom(@PathVariable("roomId") Long roomId) {
        List<Rating> ratingsForRoom = ratingRepository.findRatingsByRoomId(roomId);

        return ratingsForRoom.stream()
                .peek(rating -> rating.setRunningServiceNumber(ratingServiceNumber))
                .collect(Collectors.toList());
    }
}
