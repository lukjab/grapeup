package com.grapeup.ratingservice.entity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Entity class for room rating
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "rating")
public class Rating {

    public Rating(Long id, Integer rate, String description, Long roomId) {
        this.id = id;
        this.rate = rate;
        this.description = description;
        this.roomId = roomId;
    }

    public void setRunningServiceNumber(String runningServiceNumber) {
        this.runningServiceNumber = Integer.valueOf(runningServiceNumber);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "rate")
    private Integer rate;

    @Column(name = "description")
    private String description;

    @Column(name = "room_id")
    private Long roomId;

    @Transient
    private Integer runningServiceNumber;
}
