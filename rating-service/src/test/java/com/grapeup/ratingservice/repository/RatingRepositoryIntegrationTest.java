package com.grapeup.ratingservice.repository;

import com.grapeup.ratingservice.entity.Rating;
import com.grapeup.ratingservice.utils.RatingTestData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RatingRepositoryIntegrationTest {

    @Autowired
    private RatingRepository ratingRepository;

    @Test
    public void shouldGetAllRatings() {
        //when
        List<Rating> result = ratingRepository.findAll();
        //then
        assertEquals(6, result.size());
    }

    @Test
    public void shouldGetRatingsForRoom() {
        //given
        Long roomId = 1L;
        List<Rating> expectedRatings = RatingTestData.createExpectedRatingsForRoom();
        //when
        List<Rating> result = ratingRepository.findRatingsByRoomId(roomId);
        //then
        assertThat(result).containsExactly(expectedRatings.get(0), expectedRatings.get(1));
    }

}