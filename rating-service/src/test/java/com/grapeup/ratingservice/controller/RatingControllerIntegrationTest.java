package com.grapeup.ratingservice.controller;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static com.grapeup.ratingservice.utils.RatingTestData.*;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for {@link RatingController}
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class RatingControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @BeforeClass
    public static void init() {
        //given
        System.setProperty("RATING_SERVICE_NUMBER", "123");
    }

    @Test
    public void shouldGetAllRatings() throws Exception {
        //given
        String expectedRatings = createExpectedRatingsJSON(createExpectedRatings());
        //when && then
        mockMvc.perform(get(RATINGS_URL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(expectedRatings));
    }

    @Test
    public void shouldGetRatingsForRoom() throws Exception {
        //given
        String expectedRatings = createExpectedRatingsJSON(createExpectedRatingsForRoom());
        //when && then
        mockMvc.perform(get(RATINGS_FOR_ROOM_1_URL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(expectedRatings));
    }

    @Test
    public void shouldSetCorrectRatingServiceNumber() throws Exception {
        //when && then
        mockMvc.perform(get(RATINGS_URL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[0].runningServiceNumber", is(123)));
    }
}
