package com.grapeup.ratingservice.controller;

import com.grapeup.ratingservice.entity.Rating;
import com.grapeup.ratingservice.repository.RatingRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import static com.grapeup.ratingservice.utils.RatingTestData.createExpectedRatings;
import static com.grapeup.ratingservice.utils.RatingTestData.createExpectedRatingsForRoom;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class RatingControllerTest {

    private RatingController ratingController;

    @Mock
    private RatingRepository ratingRepository;

    @Before
    public void init() {
        initMocks(this);
        ratingController = new RatingController(ratingRepository, "111");
    }

    @Test
    public void shouldGetAllRatings() {
        //given
        List<Rating> expectedRatings = createExpectedRatings();
        when(ratingRepository.findAll()).thenReturn(expectedRatings);
        //when
        List<Rating> result = ratingController.getAllRatings();
        //then
        assertEquals(expectedRatings, result);
        verify(ratingRepository, times(1)).findAll();
    }

    @Test
    public void shouldGetRatingsForRoom() {
        //given
        List<Rating> expectedRatings = createExpectedRatingsForRoom();
        Long roomId = 1L;
        when(ratingRepository.findRatingsByRoomId(roomId)).thenReturn(expectedRatings);
        //when
        List<Rating> result = ratingController.getRatingsForRoom(roomId);
        //then
        assertEquals(expectedRatings, result);
        verify(ratingRepository, times(1)).findRatingsByRoomId(roomId);
    }

}