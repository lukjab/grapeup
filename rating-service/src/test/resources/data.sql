INSERT INTO rating
VALUES (1, 5, 'Great room', 1);

INSERT INTO rating
VALUES (2, 4, 'Nice room but air conditioning doesnt work', 1);

INSERT INTO rating
VALUES (3, 1, 'Messy, hot room, we had bed bugs', 2);

INSERT INTO rating
VALUES (4, 2, 'Very bad experience, we found dead body in the closet', 2);

INSERT INTO rating
VALUES (5, 4, 'Nice room with nice view, could be bigger', 3);

INSERT INTO rating
VALUES (6, 3, 'It was not bad but toilet didnt flush', 3);