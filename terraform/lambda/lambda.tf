data "archive_file" "jar_file" {
  type = "zip"
  source_file = "lambda/target/hello-dev.jar"
  output_path = "lambda/target/hello-dev.zip"
}

resource "aws_lambda_function" "java_lambda_function" {
  function_name = "java_lambda_function"
  filename = var.java_lambda_filename
  handler = var.lambda_function_handler
  role = aws_iam_role.cloudwatch-to-slack-role.arn
  runtime = var.lambda_runtime
  memory_size = 1024
  source_code_hash = data.archive_file.jar_file.output_base64sha256
}

resource "aws_lambda_permission" "java_lambda_function" {
  statement_id = "AllowAPIGatewayInvoke"
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.java_lambda_function.function_name
  principal = "apigateway.amazonaws.com"
  source_arn = "${aws_api_gateway_deployment.java_lambda_deploy.execution_arn}/*/*"
}