resource "aws_iam_role" "cloudwatch-to-slack-role" {
  name = "cloudwatch-to-slack-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_policy" "log-policy" {
  name = "log-policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "cloudwatch:PutMetricData",
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "*"
    }
  ]
}
EOF

}

resource "aws_iam_policy_attachment" "role-policy-attachement" {
  name = "role-policy-attachement"
  roles = [aws_iam_role.cloudwatch-to-slack-role.name]
  policy_arn = aws_iam_policy.log-policy.arn
}


