variable "region" {
  description = "Default AWS region"
  default = "us-east-2"
}

variable "java_lambda_filename" {
  default = "lambda/target/hello-dev.jar"
}

variable "lambda_function_handler" {
  default = "com.serverless.Handler"
}

variable "lambda_runtime" {
  default = "java11"
}

variable "api_path" {
  default = "{proxy+}"
}
