/*
Even though structures in H2 are created basing on Java entity classes I like to see data model with plain SQL;
I've written below CREATE statements to resemble 1:1 Java model
*/

CREATE TABLE IF NOT EXISTS room
(
    id        BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    room_type VARCHAR(9)                        NOT NULL
);

CREATE TABLE IF NOT EXISTS reservation
(
    id               BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    user_name        VARCHAR(255)                      NOT NULL,
    number_of_people INTEGER                           NOT NULL,
    start_date       DATE                              NOT NULL,
    end_date         DATE                              NOT NULL,
    room_id          BIGINT DEFAULT NULL,

    CONSTRAINT FK_ROOM FOREIGN KEY (room_id) REFERENCES room (id)
);

INSERT INTO room(id, room_type)
VALUES (1, 'BASIC');
INSERT INTO room(id, room_type)
VALUES (2, 'BASIC');
INSERT INTO room(id, room_type)
VALUES (3, 'BASIC');
INSERT INTO room(id, room_type)
VALUES (4, 'SUITE');
INSERT INTO room(id, room_type)
VALUES (5, 'SUITE');
INSERT INTO room(id, room_type)
VALUES (6, 'PENTHOUSE');
INSERT INTO room(id, room_type)
VALUES (7, 'PENTHOUSE');

INSERT INTO reservation(id, user_name, number_of_people, start_date, end_date, room_id)
VALUES (1, 'user1', 4, PARSEDATETIME('01-01-2020', 'dd-MM-yyyy'), PARSEDATETIME('01-05-2020', 'dd-MM-yyyy'), 1);

INSERT INTO reservation(id, user_name, number_of_people, start_date, end_date, room_id)
VALUES (2, 'user2', 6, PARSEDATETIME('01-02-2020', 'dd-MM-yyyy'), PARSEDATETIME('10-02-2020', 'dd-MM-yyyy'), 4);

INSERT INTO reservation(id, user_name, number_of_people, start_date, end_date, room_id)
VALUES (3, 'user3', 3, PARSEDATETIME('01-03-2020', 'dd-MM-yyyy'), PARSEDATETIME('01-04-2020', 'dd-MM-yyyy'), 2);

INSERT INTO reservation(id, user_name, number_of_people, start_date, end_date, room_id)
VALUES (4, 'user1', 4, PARSEDATETIME('01-01-2022', 'dd-MM-yyyy'), PARSEDATETIME('01-05-2022', 'dd-MM-yyyy'), 1);