package com.grape.hotelreservation.repository;

import com.grape.hotelreservation.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * DAO class for reservations
 */
@Repository
@Transactional
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    @Query("SELECT r FROM Reservation r WHERE r.room.id = :roomId")
    List<Reservation> findReservationsForRoom(@Param("roomId") Long roomId);

}
