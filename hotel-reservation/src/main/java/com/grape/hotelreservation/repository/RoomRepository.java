package com.grape.hotelreservation.repository;

import com.grape.hotelreservation.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

/**
 * DAO class for room
 */
@Repository
@Transactional
public interface RoomRepository extends JpaRepository<Room, Long> {

    @Query("SELECT ro FROM Room ro WHERE ro.id NOT IN (SELECT re.room.id FROM Reservation re WHERE re.startDate < :endDate AND re.endDate > :startDate)")
    List<Room> findAvailableRoomsInTimePeriod(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

}
