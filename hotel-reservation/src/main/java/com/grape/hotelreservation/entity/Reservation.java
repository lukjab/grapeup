package com.grape.hotelreservation.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Entity class representing reservation
 */
@Entity
@Table(name = "reservation")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class Reservation {

    public Reservation(Reservation reservation, Long id) {
        this.id = id;
        this.userName = reservation.getUserName();
        this.numberOfPeople = reservation.getNumberOfPeople();
        this.startDate = reservation.getStartDate();
        this.endDate = reservation.getEndDate();
        this.room = reservation.getRoom();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "user_name", nullable = false)
    private String userName;

    @Column(name = "number_of_people", nullable = false)
    private Integer numberOfPeople;

    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    @Column(name = "end_date", nullable = false)
    private LocalDate endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    private Room room;

}
