package com.grape.hotelreservation.entity;

import com.grape.hotelreservation.constants.RoomType;
import lombok.*;

import javax.persistence.*;

/**
 * Entity class representing room
 */
@Entity
@Table(name = "room")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "room_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private RoomType roomType;

}
