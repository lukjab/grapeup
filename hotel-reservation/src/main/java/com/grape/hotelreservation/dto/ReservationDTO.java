package com.grape.hotelreservation.dto;

import com.grape.hotelreservation.dto.validation.ValidEndDate;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

import static com.grape.hotelreservation.constants.DateFormat.DATE_REGEXP_DD_MM_YYYY;

/**
 * Data transfer object for reservation request
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@EqualsAndHashCode
@ValidEndDate
public class ReservationDTO implements Serializable {

    private static final long serialVersionUID = 8162936006930668886L;

    @NotNull
    @NotBlank
    private String userName;
    @NotNull
    private Integer numberOfPeople;
    @NotNull
    @NotBlank
    @Pattern(regexp = DATE_REGEXP_DD_MM_YYYY)
    private String startDate;
    @NotNull
    @NotBlank
    @Pattern(regexp = DATE_REGEXP_DD_MM_YYYY)
    private String endDate;
    private Long roomId;
}
