package com.grape.hotelreservation.dto.validation;

import com.grape.hotelreservation.dto.ReservationDTO;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.util.regex.Pattern;

import static com.grape.hotelreservation.constants.DateFormat.DATE_FORMATTER_DD_MM_YYYY;
import static com.grape.hotelreservation.constants.DateFormat.DATE_REGEXP_DD_MM_YYYY;


/**
 * Validator for checking if end date is after start date;
 * It validates only if both start and end date are not blank and not nulls and in correct format
 */
public class ReservationDatesValidator implements ConstraintValidator<ValidEndDate, ReservationDTO> {

    @Override
    public void initialize(ValidEndDate constraintAnnotation) {
    }

    @Override
    public boolean isValid(ReservationDTO reservationDTO, ConstraintValidatorContext constraintValidatorContext) {

        if (StringUtils.isEmpty(reservationDTO.getStartDate()) ||
                StringUtils.isEmpty(reservationDTO.getEndDate()) ||
                !Pattern.matches(DATE_REGEXP_DD_MM_YYYY, reservationDTO.getStartDate()) ||
                !Pattern.matches(DATE_REGEXP_DD_MM_YYYY, reservationDTO.getEndDate())) {
            return true;
        }

        final LocalDate endDate = LocalDate.parse(reservationDTO.getEndDate(), DATE_FORMATTER_DD_MM_YYYY);
        final LocalDate startDate = LocalDate.parse(reservationDTO.getStartDate(), DATE_FORMATTER_DD_MM_YYYY);

        return endDate.isAfter(startDate);
    }

}