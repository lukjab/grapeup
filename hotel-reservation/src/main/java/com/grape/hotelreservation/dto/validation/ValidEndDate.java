package com.grape.hotelreservation.dto.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static com.grape.hotelreservation.constants.ErrorMessages.END_DATE_MUST_BE_AFTER_START_DATE;

@Documented
@Constraint(validatedBy = ReservationDatesValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidEndDate {

    String message() default END_DATE_MUST_BE_AFTER_START_DATE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String fieldName() default "endDate";
}
