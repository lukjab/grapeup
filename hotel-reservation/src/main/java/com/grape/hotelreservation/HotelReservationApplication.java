package com.grape.hotelreservation;

import com.grape.hotelreservation.dto.ReservationDTO;
import com.grape.hotelreservation.entity.Reservation;
import com.grape.hotelreservation.repository.RoomRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;

import static com.grape.hotelreservation.helper.ModelMapperConverter.prepareCreateReservationConverter;
import static com.grape.hotelreservation.helper.ModelMapperConverter.prepareLocalDateStringConverter;

@SpringBootApplication
@EnableEurekaClient
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class HotelReservationApplication {

    private RoomRepository roomRepository;

    public static void main(String[] args) {
        SpringApplication.run(HotelReservationApplication.class, args);
    }

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        modelMapper.getConfiguration().setFieldMatchingEnabled(true)
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE);

        Converter<LocalDate, String> localDateStringConverter = prepareLocalDateStringConverter();
        modelMapper.addConverter(localDateStringConverter);

        Converter<ReservationDTO, Reservation> reservationConverter = prepareCreateReservationConverter(roomRepository);
        modelMapper.addConverter(reservationConverter);

        return modelMapper;
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}