package com.grape.hotelreservation.service.reservation;

import com.grape.hotelreservation.entity.Reservation;

import java.util.List;

/**
 * Service responsible for CRUD logic related to reservations
 */
public interface ReservationService {

    List<Reservation> getAllReservations();
    Reservation getReservationById(Long reservationId);
    Reservation saveReservation(Reservation createReservationRequest);
    void deleteReservationById(Long reservationId);
    List<Reservation> getAllReservationsForRoom(Long roomId);

}
