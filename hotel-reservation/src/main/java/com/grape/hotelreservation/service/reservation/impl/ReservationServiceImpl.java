package com.grape.hotelreservation.service.reservation.impl;

import com.grape.hotelreservation.entity.Reservation;
import com.grape.hotelreservation.exceptions.NotFoundException;
import com.grape.hotelreservation.repository.ReservationRepository;
import com.grape.hotelreservation.service.reservation.ReservationService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.grape.hotelreservation.constants.ErrorMessages.RESERVATION_NOT_FOUND_FOR_ID;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ReservationServiceImpl implements ReservationService {

    private ReservationRepository reservationRepository;

    @Override
    public List<Reservation> getAllReservations() {
        return reservationRepository.findAll();
    }

    @Override
    public Reservation getReservationById(Long reservationId) {
        return reservationRepository.findById(reservationId)
                .orElseThrow(() -> new NotFoundException(RESERVATION_NOT_FOUND_FOR_ID));
    }

    @Override
    public Reservation saveReservation(Reservation createReservationRequest) {
        return reservationRepository.save(createReservationRequest);
    }

    @Override
    public void deleteReservationById(Long reservationId) {
        try {
            reservationRepository.deleteById(reservationId);
        } catch (EmptyResultDataAccessException ex) {
            //explicitly do nothing - we want to return status 200 in case when such id does not exist
        }
    }

    @Override
    public List<Reservation> getAllReservationsForRoom(Long roomId) {
        return reservationRepository.findReservationsForRoom(roomId);
    }
}
