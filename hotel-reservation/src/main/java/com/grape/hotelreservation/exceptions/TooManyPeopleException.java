package com.grape.hotelreservation.exceptions;

/**
 * Exception thrown when user provided too many people in his reservation request
 */
public class TooManyPeopleException extends RuntimeException {

    private static final long serialVersionUID = -2149372257289485105L;

    public TooManyPeopleException(String message) {
        super(message);
    }

}
