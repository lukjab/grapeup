package com.grape.hotelreservation.exceptions;

/**
 * Exception thrown when reservation with provided id was not found
 */
public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = 3237796313770714975L;

    public NotFoundException(String message) {
        super(message);
    }

}