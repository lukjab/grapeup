package com.grape.hotelreservation.exceptions;

/**
 * Exception thrown when no appropriate room was found
 */
public class NoAvailableRoomsException extends RuntimeException {

    private static final long serialVersionUID = -8962479195559190646L;

    public NoAvailableRoomsException(String message) {
        super(message);
    }

}