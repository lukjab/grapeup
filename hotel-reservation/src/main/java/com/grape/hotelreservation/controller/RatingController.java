package com.grape.hotelreservation.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * Controller retrieving rating from rating microservice
 */
@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("/ratings")
public class RatingController {

    private RestTemplate restTemplate;

    @GetMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public String getAllRatings() {
        String ratings = restTemplate.getForObject("http://rating-service/ratings", String.class);

        return ratings; //todo implement getting ratings from service discovery registered rating microservice instances
    }

}
