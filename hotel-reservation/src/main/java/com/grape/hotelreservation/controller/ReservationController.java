package com.grape.hotelreservation.controller;

import com.grape.hotelreservation.dto.ReservationDTO;
import com.grape.hotelreservation.entity.Reservation;
import com.grape.hotelreservation.service.reservation.ReservationService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller class responsible for CRUD operations related to reservation
 */
@RestController
@RequestMapping("/reservations")
public class ReservationController {

    private final ModelMapper modelMapper;
    private final ReservationService reservationService;
    private final String reservationServiceURL;

    @Autowired
    public ReservationController(ModelMapper modelMapper, ReservationService reservationService, @Value("${reservation.service.url}") String reservationServiceURL) {
        this.modelMapper = modelMapper;
        this.reservationService = reservationService;
        this.reservationServiceURL = reservationServiceURL;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<ReservationDTO> getAllReservations() {
        List<Reservation> reservations = reservationService.getAllReservations();

        return reservations.stream()
                .map(reservation -> modelMapper.map(reservation, ReservationDTO.class))
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public ReservationDTO getReservationById(@PathVariable("id") Long reservationId) {
        Reservation reservation = reservationService.getReservationById(reservationId);

        return modelMapper.map(reservation, ReservationDTO.class);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public String createReservation(@RequestBody @Valid ReservationDTO createReservationRequest) {
        Reservation reservationToCreate = modelMapper.map(createReservationRequest, Reservation.class);

        Reservation createdReservation = reservationService.saveReservation(reservationToCreate);

        return reservationServiceURL.concat(createdReservation.getId().toString());
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public ReservationDTO updateReservation(@RequestBody @Valid ReservationDTO updateReservationRequest, @PathVariable("id") Long reservationId) {
        Reservation reservationToUpdate = new Reservation(modelMapper.map(updateReservationRequest, Reservation.class), reservationId);

        Reservation updatedReservation = reservationService.saveReservation(reservationToUpdate);

        return modelMapper.map(updatedReservation, ReservationDTO.class);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteReservation(@PathVariable("id") Long reservationId) {
        reservationService.deleteReservationById(reservationId);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = {"roomNumber"})
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<ReservationDTO> getAllReservationsForRoom(@RequestParam(value = "roomNumber") Long roomId) {
        List<Reservation> reservationsForRoom = reservationService.getAllReservationsForRoom(roomId);

        return reservationsForRoom.stream()
                .map(reservation -> modelMapper.map(reservation, ReservationDTO.class))
                .collect(Collectors.toList());
    }
}
