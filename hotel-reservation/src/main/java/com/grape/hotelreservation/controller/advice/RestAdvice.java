package com.grape.hotelreservation.controller.advice;

import com.grape.hotelreservation.exceptions.NoAvailableRoomsException;
import com.grape.hotelreservation.exceptions.NotFoundException;
import com.grape.hotelreservation.exceptions.TooManyPeopleException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * Auxiliary class used for defining global exception handling strategies
 */
@RestControllerAdvice
public class RestAdvice {

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseMessage handleNotFoundException(NotFoundException ex) {
        return new ResponseMessage(ex.getMessage());
    }

    @ExceptionHandler(TooManyPeopleException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseMessage handleTooManyPeopleException(TooManyPeopleException ex) {
        return new ResponseMessage(ex.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = "";
            try {
                fieldName = ((FieldError) error).getField();
            } catch (ClassCastException e) {
                fieldName = error.getArguments()[1].toString();
            }
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        return errors;
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(NoAvailableRoomsException.class)
    public ResponseMessage handleNoAvailableRoomsException(NoAvailableRoomsException ex) {
        return new ResponseMessage(ex.getMessage());
    }

}