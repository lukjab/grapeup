package com.grape.hotelreservation.helper;

import com.grape.hotelreservation.constants.RoomType;
import com.grape.hotelreservation.dto.ReservationDTO;
import com.grape.hotelreservation.entity.Reservation;
import com.grape.hotelreservation.entity.Room;
import com.grape.hotelreservation.exceptions.NoAvailableRoomsException;
import com.grape.hotelreservation.repository.RoomRepository;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.grape.hotelreservation.constants.DateFormat.DATE_FORMATTER_DD_MM_YYYY;
import static com.grape.hotelreservation.constants.ErrorMessages.NO_AVAILABLE_ROOMS_FOR_TIME_PERIOD;

/**
 * Auxiliary class used for model mapper initialization
 */
public class ModelMapperConverter {

    public static Converter<LocalDate, String> prepareLocalDateStringConverter() {
        return new Converter<LocalDate, String>() {
            @Override
            public String convert(MappingContext<LocalDate, String> mappingContext) {
                String destination = mappingContext.getSource().format(DATE_FORMATTER_DD_MM_YYYY);

                return destination;
            }
        };
    }

    public static Converter<ReservationDTO, Reservation> prepareCreateReservationConverter(RoomRepository roomRepository) {
        return new Converter<ReservationDTO, Reservation>() {
            @Override
            public Reservation convert(MappingContext<ReservationDTO, Reservation> mappingContext) {
                ReservationDTO source = mappingContext.getSource();
                Optional<Room> room = source.getRoomId() != null ? roomRepository.findById(source.getRoomId()) : Optional.empty();

                Reservation reservation = Reservation.builder()
                        .userName(source.getUserName())
                        .numberOfPeople(source.getNumberOfPeople())
                        .startDate(LocalDate.parse(source.getStartDate(), DATE_FORMATTER_DD_MM_YYYY))
                        .endDate(LocalDate.parse(source.getEndDate(), DATE_FORMATTER_DD_MM_YYYY))
                        .room(room.orElse(findAvailableRoomForNumberOfPeople(source, roomRepository)))
                        .build();

                return reservation;
            }
        };
    }

    private static Room findAvailableRoomForNumberOfPeople(ReservationDTO createReservationRequest, RoomRepository roomRepository) {
        RoomType neededRoomType = RoomType.getCorrectRoomType(createReservationRequest.getNumberOfPeople());

        LocalDate startDate = LocalDate.parse(createReservationRequest.getStartDate(), DATE_FORMATTER_DD_MM_YYYY);
        LocalDate endDate = LocalDate.parse(createReservationRequest.getEndDate(), DATE_FORMATTER_DD_MM_YYYY);

        List<Room> availableRooms = roomRepository.findAvailableRoomsInTimePeriod(startDate, endDate);

        return availableRooms.stream()
                .filter(room -> room.getRoomType() == neededRoomType)
                .findFirst()
                .orElse(getBiggerRoom(availableRooms, createReservationRequest));
    }

    private static Room getBiggerRoom(List<Room> availableRooms, ReservationDTO createReservationRequest) {
        return availableRooms.stream()
                .filter(room -> room.getRoomType().getPeopleLimit() >= createReservationRequest.getNumberOfPeople())
                .findFirst()
                .orElseThrow(() -> new NoAvailableRoomsException(NO_AVAILABLE_ROOMS_FOR_TIME_PERIOD));
    }

}
