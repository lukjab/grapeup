package com.grape.hotelreservation.constants;

import com.grape.hotelreservation.exceptions.TooManyPeopleException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

import static com.grape.hotelreservation.constants.ErrorMessages.TOO_MANY_PEOPLE;

/**
 * Auxiliary enum for room types
 */
@AllArgsConstructor
public enum RoomType {
    BASIC(4),
    SUITE(6),
    PENTHOUSE(8);

    @Getter
    private Integer peopleLimit;

    public static RoomType getCorrectRoomType(Integer numberOfPeople) throws TooManyPeopleException {
        return Arrays.stream(values())
                .filter(roomType -> roomType.getPeopleLimit() >= numberOfPeople)
                .findFirst()
                .orElseThrow(() -> new TooManyPeopleException(TOO_MANY_PEOPLE));
    }
}
