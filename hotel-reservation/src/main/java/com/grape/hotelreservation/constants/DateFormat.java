package com.grape.hotelreservation.constants;

import java.time.format.DateTimeFormatter;

/**
 * Auxiliary class used for holding date format
 */
public class DateFormat {
    public static final String DATE_PATTERN_DD_MM_YYYY = "dd-MM-yyyy";
    public static final DateTimeFormatter DATE_FORMATTER_DD_MM_YYYY = DateTimeFormatter.ofPattern(DATE_PATTERN_DD_MM_YYYY);
    public static final String DATE_REGEXP_DD_MM_YYYY = "[0-3]?[0-9]{1}-[0-1]?[0-9]{1}-20[2-9]{1}[0-9]{1}";
}
