package com.grape.hotelreservation.constants;

/**
 * Auxiliary class for holding specified error messages returned to caller
 */
public class ErrorMessages {
    public static final String TOO_MANY_PEOPLE = "You provided more than 8 people in reservation request. We don't have rooms that can fulfil such request.";
    public static final String RESERVATION_NOT_FOUND_FOR_ID = "Reservation with provided id was not found.";
    public static final String NO_AVAILABLE_ROOMS_FOR_TIME_PERIOD = "We don't have any available rooms that could fulfil request for provided time period";
    public static final String END_DATE_MUST_BE_AFTER_START_DATE = "End date must be after start date.";
}
