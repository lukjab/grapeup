package com.grape.hotelreservation.service.reservation;

import com.grape.hotelreservation.entity.Reservation;
import com.grape.hotelreservation.exceptions.NotFoundException;
import com.grape.hotelreservation.repository.ReservationRepository;
import com.grape.hotelreservation.service.reservation.impl.ReservationServiceImpl;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static com.grape.hotelreservation.constants.ErrorMessages.RESERVATION_NOT_FOUND_FOR_ID;
import static com.grape.hotelreservation.utils.TestData.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Test class for {@link ReservationService}
 */
public class ReservationServiceTest {

    @InjectMocks
    private ReservationServiceImpl reservationService;

    @Mock
    private ReservationRepository reservationRepository;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void shouldGetAllReservations() {
        //given
        List<Reservation> emptyReservations = createEmptyReservations();
        when(reservationRepository.findAll()).thenReturn(emptyReservations);
        //when
        List<Reservation> result = reservationService.getAllReservations();
        //then
        assertEquals(emptyReservations.size(), result.size());
        verify(reservationRepository, times(1)).findAll();
    }

    @Test
    public void shouldGetSingleReservationById() throws IOException, ParseException {
        //given
        Reservation dummyReservation = createDummyReservation();
        when(reservationRepository.findById(DUMMY_RESERVATION_ID)).thenReturn(Optional.of(dummyReservation));
        //when
        Reservation result = reservationService.getReservationById(DUMMY_RESERVATION_ID);
        //then
        assertEquals(dummyReservation, result);
        verify(reservationRepository, times(1)).findById(DUMMY_RESERVATION_ID);
    }

    @Test
    public void shouldThrowExceptionIfReservationNotFoundById() {
        //given
        when(reservationRepository.findById(DUMMY_RESERVATION_ID)).thenReturn(Optional.empty());
        try {
            //when
            reservationService.getReservationById(DUMMY_RESERVATION_ID);
            fail();
        } catch (NotFoundException ex) {
            //then
            assertEquals(RESERVATION_NOT_FOUND_FOR_ID, ex.getMessage());
        }
        verify(reservationRepository, times(1)).findById(DUMMY_RESERVATION_ID);
    }

    @Test
    public void shouldSaveReservation() {
        //when
        reservationService.saveReservation(RESERVATION_TO_CREATE);
        //then
        verify(reservationRepository, times(1)).save(RESERVATION_TO_CREATE);
    }

    @Test
    public void shouldDeleteReservation() {
        //given
        Long reservationToDeleteId = 1L;
        //when
        reservationService.deleteReservationById(reservationToDeleteId);
        //then
        verify(reservationRepository, times(1)).deleteById(reservationToDeleteId);
    }

    @Test
    public void shouldGetReservationsForRoom() {
        //given
        Long roomId = 1L;
        List<Reservation> emptyReservations = createEmptyReservations();
        when(reservationRepository.findReservationsForRoom(roomId)).thenReturn(emptyReservations);
        //when
        List<Reservation> result = reservationService.getAllReservationsForRoom(roomId);
        //then
        assertEquals(emptyReservations, result);
        verify(reservationRepository, times(1)).findReservationsForRoom(roomId);
    }

}