package com.grape.hotelreservation.repository;

import com.grape.hotelreservation.entity.Reservation;
import com.grape.hotelreservation.entity.Room;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.grape.hotelreservation.constants.DateFormat.DATE_FORMATTER_DD_MM_YYYY;
import static com.grape.hotelreservation.constants.RoomType.PENTHOUSE;
import static com.grape.hotelreservation.utils.TestData.RESERVATION_TO_CREATE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Integration test for {@link ReservationRepository}
 * <p>
 * Initial test data on which test is based is specified in data.sql file
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ReservationRepositoryIntegrationTest {

    @Autowired
    private ReservationRepository reservationRepository;

    @Test
    public void shouldFindAllInitialReservations() {
        //when
        List<Reservation> result = reservationRepository.findAll();
        //then
        assertEquals(4, result.size());
    }

    @Test
    public void shouldCreateNewReservation() {
        //given
        Long expectedNewReservationId = 5L;
        //when
        reservationRepository.save(RESERVATION_TO_CREATE);
        //then
        Reservation createdReservation = reservationRepository.findById(expectedNewReservationId).get();
        assertEquals(RESERVATION_TO_CREATE, createdReservation);
    }

    @Test
    public void shouldUpdateExistingReservation() {
        //given
        final String newUserName = "newUserName";
        final Integer newNumberOfPeople = 8;
        final LocalDate newStartDate = LocalDate.parse("12-12-2020", DATE_FORMATTER_DD_MM_YYYY);
        final LocalDate newEndDate = LocalDate.parse("26-12-2020", DATE_FORMATTER_DD_MM_YYYY);
        final Room newRoom = Room.builder()
                .id(7L)
                .roomType(PENTHOUSE)
                .build();

        final Long reservationToUpdateId = 1L;

        Reservation reservationToUpdate = Reservation.builder()
                .id(reservationToUpdateId)
                .userName(newUserName)
                .numberOfPeople(newNumberOfPeople)
                .startDate(newStartDate)
                .endDate(newEndDate)
                .room(newRoom)
                .build();
        //when
        reservationRepository.saveAndFlush(reservationToUpdate);
        Reservation updatedReservation = reservationRepository.findById(reservationToUpdateId).get();
        //then
        assertEquals(newNumberOfPeople, updatedReservation.getNumberOfPeople());
        assertEquals(newStartDate, updatedReservation.getStartDate());
        assertEquals(newEndDate, updatedReservation.getEndDate());
        assertEquals(newRoom, updatedReservation.getRoom());
    }

    @Test
    public void shouldDeleteExistingReservation() {
        //given
        final Long reservationToDeleteId = 1L;
        //when
        reservationRepository.deleteById(reservationToDeleteId);
        //when
        Optional<Reservation> foundReservation = reservationRepository.findById(reservationToDeleteId);
        assertTrue(foundReservation.isEmpty());
    }

    @Test
    public void shouldFindAllReservationsForRoom() {
        //given
        Long roomId = 1L;
        //when
        List<Reservation> result = reservationRepository.findReservationsForRoom(roomId);
        //then
        List<Long> foundReservationsIds = result.stream()
                .map(Reservation::getId)
                .collect(Collectors.toList());
        assertThat(foundReservationsIds).containsExactly(1L, 4L);
    }

    @Test
    public void shouldFindNoReservationsForRoom() {
        //given
        Long roomId = 3L;
        //when
        List<Reservation> result = reservationRepository.findReservationsForRoom(roomId);
        //then
        List<Long> foundReservationsIds = result.stream()
                .map(Reservation::getId)
                .collect(Collectors.toList());
        assertThat(foundReservationsIds).isEmpty();
    }
}