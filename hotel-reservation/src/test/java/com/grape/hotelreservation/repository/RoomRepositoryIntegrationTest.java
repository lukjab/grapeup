package com.grape.hotelreservation.repository;

import com.grape.hotelreservation.entity.Room;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static com.grape.hotelreservation.constants.DateFormat.DATE_FORMATTER_DD_MM_YYYY;
import static com.grape.hotelreservation.constants.RoomType.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Integration tests for {@link RoomRepository}
 *
 * Initial test data on which test is based is specified in data.sql file
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class RoomRepositoryIntegrationTest {

    @Autowired
    private RoomRepository roomRepository;

    @Test
    public void shouldFindAllInitialRooms() {
        //when
        List<Room> result = roomRepository.findAll();
        //then
        assertEquals(7, result.size());
    }

    @Test
    public void shouldFindAllInitialBasicRooms() {
        //when
        List<Room> result = roomRepository.findAll();
        List<Room> basicRooms = result.stream()
                .filter(room -> room.getRoomType() == BASIC)
                .collect(Collectors.toList());
        //then
        assertEquals(3, basicRooms.size());
    }

    @Test
    public void shouldFindAllInitialSuiteRooms() {
        //when
        List<Room> result = roomRepository.findAll();
        List<Room> suiteRooms = result.stream()
                .filter(room -> room.getRoomType() == SUITE)
                .collect(Collectors.toList());
        //then
        assertEquals(2, suiteRooms.size());
    }

    @Test
    public void shouldFindAllInitialPenthouseRooms() {
        //when
        List<Room> result = roomRepository.findAll();
        List<Room> penthouseRooms = result.stream()
                .filter(room -> room.getRoomType() == PENTHOUSE)
                .collect(Collectors.toList());
        //then
        assertEquals(2, penthouseRooms.size());
    }

    @Test
    public void shouldFindAvailableRoomsInSpecifiedTimePeriods() {
        //given
        //Period 1
        LocalDate startDatePeriod1 = LocalDate.parse("12-12-2019", DATE_FORMATTER_DD_MM_YYYY);
        LocalDate endDatePeriod1 = LocalDate.parse("30-12-2019", DATE_FORMATTER_DD_MM_YYYY);
        //Period 2
        LocalDate startDatePeriod2 = LocalDate.parse("02-01-2020", DATE_FORMATTER_DD_MM_YYYY);
        LocalDate endDatePeriod2 = LocalDate.parse("30-01-2020", DATE_FORMATTER_DD_MM_YYYY);
        //Period 3
        LocalDate startDatePeriod3 = LocalDate.parse("05-02-2020", DATE_FORMATTER_DD_MM_YYYY);
        LocalDate endDatePeriod3 = LocalDate.parse("05-03-2020", DATE_FORMATTER_DD_MM_YYYY);
        //Period 4
        LocalDate startDatePeriod4 = LocalDate.parse("31-12-2019", DATE_FORMATTER_DD_MM_YYYY);
        LocalDate endDatePeriod4 = LocalDate.parse("02-05-2020", DATE_FORMATTER_DD_MM_YYYY);
        //Period 5
        LocalDate startDatePeriod5 = LocalDate.parse("02-05-2020", DATE_FORMATTER_DD_MM_YYYY);
        LocalDate endDatePeriod5 = LocalDate.parse("12-05-2020", DATE_FORMATTER_DD_MM_YYYY);
        //Period 6
        LocalDate startDatePeriod6 = LocalDate.parse("11-02-2020", DATE_FORMATTER_DD_MM_YYYY);
        LocalDate endDatePeriod6 = LocalDate.parse("27-02-2020", DATE_FORMATTER_DD_MM_YYYY);
        //when
        List<Room> resultPeriod1 = roomRepository.findAvailableRoomsInTimePeriod(startDatePeriod1, endDatePeriod1);
        List<Room> resultPeriod2 = roomRepository.findAvailableRoomsInTimePeriod(startDatePeriod2, endDatePeriod2);
        List<Room> resultPeriod3 = roomRepository.findAvailableRoomsInTimePeriod(startDatePeriod3, endDatePeriod3);
        List<Room> resultPeriod4 = roomRepository.findAvailableRoomsInTimePeriod(startDatePeriod4, endDatePeriod4);
        List<Room> resultPeriod5 = roomRepository.findAvailableRoomsInTimePeriod(startDatePeriod5, endDatePeriod5);
        List<Room> resultPeriod6 = roomRepository.findAvailableRoomsInTimePeriod(startDatePeriod6, endDatePeriod6);

        List<Long> availableRoomIdsPeriod1 = resultPeriod1.stream()
                .map(Room::getId)
                .collect(Collectors.toList());
        List<Long> availableRoomIdsPeriod2 = resultPeriod2.stream()
                .map(Room::getId)
                .collect(Collectors.toList());
        List<Long> availableRoomIdsPeriod3 = resultPeriod3.stream()
                .map(Room::getId)
                .collect(Collectors.toList());
        List<Long> availableRoomIdsPeriod4 = resultPeriod4.stream()
                .map(Room::getId)
                .collect(Collectors.toList());
        List<Long> availableRoomIdsPeriod5 = resultPeriod5.stream()
                .map(Room::getId)
                .collect(Collectors.toList());
        List<Long> availableRoomIdsPeriod6 = resultPeriod6.stream()
                .map(Room::getId)
                .collect(Collectors.toList());
        //then
        assertThat(availableRoomIdsPeriod1).containsExactly(1L, 2L, 3L, 4L, 5L, 6L, 7L);
        assertThat(availableRoomIdsPeriod2).containsExactly(2L, 3L, 4L, 5L, 6L, 7L);
        assertThat(availableRoomIdsPeriod3).containsExactly(3L, 5L, 6L, 7L);
        assertThat(availableRoomIdsPeriod4).containsExactly(3L, 5L, 6L, 7L);
        assertThat(availableRoomIdsPeriod5).containsExactly(1L, 2L, 3L, 4L, 5L, 6L, 7L);
        assertThat(availableRoomIdsPeriod6).containsExactly(2L, 3L, 4L, 5L, 6L, 7L);
    }

}