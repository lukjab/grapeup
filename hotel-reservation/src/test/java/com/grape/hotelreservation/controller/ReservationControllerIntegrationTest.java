package com.grape.hotelreservation.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static com.grape.hotelreservation.constants.ErrorMessages.*;
import static com.grape.hotelreservation.utils.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integrations tests for {@link ReservationController}
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class ReservationControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnAllReservations() throws Exception {
        //given
        String expectedReservations = getExpectedReservationsJSON();
        //when && then
        mockMvc.perform(get(RESERVATIONS_URL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(expectedReservations));
    }

    @Test
    public void shouldReturnReservationById() throws Exception {
        //given
        String dummyReservation = createDummyReservationDTO();
        //when && then
        mockMvc.perform(get(RESERVATION_BY_ID_URL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(dummyReservation));
    }

    @Test
    public void shouldReturnNotFoundExceptionForNonExistingReservationId() throws Exception {
        //when && then
        mockMvc.perform(get(RESERVATION_BY_NON_EXISTING_ID_URL))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message", is(RESERVATION_NOT_FOUND_FOR_ID)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
    }

    @Test
    public void shouldCreateReservation() throws Exception {
        //given
        String reservationToCreate = createReservationToCreateDTO();
        //when && then
        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reservationToCreate))
                .andExpect(status().isCreated())
                .andExpect(content().string(CREATED_RESERVATION_URL));
    }

    @Test
    public void shouldFailValidationForNoUserNameOnReservationCreation() throws Exception {
        //given
        String reservationWithoutUserName = createReservationDTOJSON(null, RESERVATION_TO_CREATE_DTO.getNumberOfPeople(), RESERVATION_TO_CREATE_DTO.getStartDate(), RESERVATION_TO_CREATE_DTO.getEndDate(), RESERVATION_TO_CREATE_DTO.getRoomId());
        //when && then
        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reservationWithoutUserName))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.userName", notNullValue()));
    }

    @Test
    public void shouldFailValidationForBlankUserNameOnReservationCreation() throws Exception {
        //given
        String reservationWithBlankUserName = createReservationDTOJSON("", RESERVATION_TO_CREATE_DTO.getNumberOfPeople(), RESERVATION_TO_CREATE_DTO.getStartDate(), RESERVATION_TO_CREATE_DTO.getEndDate(), RESERVATION_TO_CREATE_DTO.getRoomId());
        //when && then
        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reservationWithBlankUserName))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.userName", notNullValue()));
    }

    @Test
    public void shouldFailValidationForNoNumberOfPeopleProvidedOnReservationCreation() throws Exception {
        //given
        String reservationWithoutNumberOfPeople = createReservationDTOJSON("userName", null, RESERVATION_TO_CREATE_DTO.getStartDate(), RESERVATION_TO_CREATE_DTO.getEndDate(), RESERVATION_TO_CREATE_DTO.getRoomId());
        //when && then
        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reservationWithoutNumberOfPeople))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.numberOfPeople", notNullValue()));
    }

    @Test
    public void shouldFailValidationForTooManyPeopleProvidedOnReservationCreation() throws Exception {
        //given
        String reservationExceedingPeopleLimit = createReservationDTOJSON("userName", 10, RESERVATION_TO_CREATE_DTO.getStartDate(), RESERVATION_TO_CREATE_DTO.getEndDate(), RESERVATION_TO_CREATE_DTO.getRoomId());
        //when && then
        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reservationExceedingPeopleLimit))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is(TOO_MANY_PEOPLE)));
    }

    @Test
    public void shouldFailValidationForNoStartDateOnReservationCreation() throws Exception {
        //given
        String reservationWithoutStartDate = createReservationDTOJSON("userName", 6, null, RESERVATION_TO_CREATE_DTO.getEndDate(), RESERVATION_TO_CREATE_DTO.getRoomId());
        //when && then
        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reservationWithoutStartDate))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.startDate", notNullValue()));
    }

    @Test
    public void shouldFailValidationForBlankStartDateOnReservationCreation() throws Exception {
        //given
        String reservationWithBlankStartDate = createReservationDTOJSON("userName", 6, "", RESERVATION_TO_CREATE_DTO.getEndDate(), RESERVATION_TO_CREATE_DTO.getRoomId());
        //when && then
        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reservationWithBlankStartDate))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.startDate", notNullValue()));
    }

    @Test
    public void shouldFailValidationForWrongFormatForStartDateOnReservationCreation() throws Exception {
        //given
        String reservationWithStartDateInWrongFormat = createReservationDTOJSON("userName", 6, "91-11-2020", RESERVATION_TO_CREATE_DTO.getEndDate(), RESERVATION_TO_CREATE_DTO.getRoomId());
        //when && then
        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reservationWithStartDateInWrongFormat))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.startDate", notNullValue()));
    }

    @Test
    public void shouldFailValidationForNoEndDateOnReservationCreation() throws Exception {
        //given
        String reservationWithoutEndDate = createReservationDTOJSON("userName", 6, RESERVATION_TO_CREATE_DTO.getStartDate(), null, RESERVATION_TO_CREATE_DTO.getRoomId());
        //when && then
        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reservationWithoutEndDate))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.endDate", notNullValue()));
    }

    @Test
    public void shouldFailValidationForBlankEndDateOnReservationCreation() throws Exception {
        //given
        String reservationWithBlackEndDate = createReservationDTOJSON("userName", 6, RESERVATION_TO_CREATE_DTO.getStartDate(), "", RESERVATION_TO_CREATE_DTO.getRoomId());
        //when && then
        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reservationWithBlackEndDate))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.endDate", notNullValue()));
    }

    @Test
    public void shouldFailValidationForWrongFormatForEndDateOnReservationCreation() throws Exception {
        //given
        String reservationWithEndDateInWrongFormat = createReservationDTOJSON("userName", 6, RESERVATION_TO_CREATE_DTO.getStartDate(), "20-122-2022", RESERVATION_TO_CREATE_DTO.getRoomId());
        //when && then
        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reservationWithEndDateInWrongFormat))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.endDate", notNullValue()));
    }

    @Test
    public void shouldFailValidationForEndDateEqualToStartDateOnReservationCreation() throws Exception {
        //given
        String reservationWithEndDateInWrongFormat = createReservationDTOJSON("userName", 6, RESERVATION_TO_CREATE_DTO.getStartDate(), RESERVATION_TO_CREATE_DTO.getStartDate(), RESERVATION_TO_CREATE_DTO.getRoomId());
        //when && then
        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reservationWithEndDateInWrongFormat))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.endDate", is(END_DATE_MUST_BE_AFTER_START_DATE)));
    }

    @Test
    public void shouldFailValidationForEndDateBeforeStartDateOnReservationCreation() throws Exception {
        //given
        String reservationWithEndDateInWrongFormat = createReservationDTOJSON("userName", 6, RESERVATION_TO_CREATE_DTO.getEndDate(), RESERVATION_TO_CREATE_DTO.getStartDate(), RESERVATION_TO_CREATE_DTO.getRoomId());
        //when && then
        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reservationWithEndDateInWrongFormat))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.endDate", is(END_DATE_MUST_BE_AFTER_START_DATE)));
    }

    @Test
    public void shouldThrowExceptionForNoAvailableRoomsForTimePeriod() throws Exception {
        //given
        String createReservationForPenthouse = createReservationDTOJSON("username11", 8, "01-01-2020", "20-01-2020", null);
        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(createReservationForPenthouse))
                .andExpect(status().isCreated());

        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(createReservationForPenthouse))
                .andExpect(status().isCreated());
        //when && then
        mockMvc.perform(post(RESERVATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(createReservationForPenthouse))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message", is(NO_AVAILABLE_ROOMS_FOR_TIME_PERIOD)));
    }

    @Test
    public void shouldUpdateReservation() throws Exception {
        //given
        String newEndDate = "15-02-2020";
        Integer newNumberOfPeople = 8;

        String updateReservationRequest = createReservationDTOJSON("user2", newNumberOfPeople, "01-02-2020", newEndDate, 4L);

        //when && then
        mockMvc.perform(put(UPDATE_RESERVATION_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(updateReservationRequest))
                .andExpect(status().isOk())
                .andExpect(content().json(updateReservationRequest));

        mockMvc.perform(get(UPDATE_RESERVATION_URL))
                .andExpect(content().json(updateReservationRequest));
    }

    @Test
    public void shouldDeleteReservation() throws Exception {
        //given
        mockMvc.perform(get(RESERVATION_BY_ID_URL))
                .andExpect(status().isOk());
        //when
        mockMvc.perform(delete(RESERVATION_BY_ID_URL))
                .andExpect(status().isOk());
        //then
        mockMvc.perform(get(RESERVATION_BY_ID_URL))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message", is(RESERVATION_NOT_FOUND_FOR_ID)));
    }

    @Test
    public void shouldReturnOkStatusForDeletingNonExistingReservation() throws Exception {
        //when && then
        mockMvc.perform(delete(RESERVATION_BY_NON_EXISTING_ID_URL))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldGetAllReservationsForRoom() throws Exception {
        //given
        String expectedReservationsForRoom = getExpectedReservationsForRoomJSON();
        //when && then
        mockMvc.perform(get(RESERVATIONS_FOR_ROOM_URL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(expectedReservationsForRoom));
    }

}
