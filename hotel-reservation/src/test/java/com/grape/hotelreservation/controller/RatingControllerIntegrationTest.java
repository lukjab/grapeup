package com.grape.hotelreservation.controller;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static com.grape.hotelreservation.utils.RatingTestData.createExpectedRatings;
import static com.grape.hotelreservation.utils.RatingTestData.createExpectedRatingsJSON;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * Integration test for {@link RatingController}
 * <p>
 * It requires all services working - config, discovery and at least one instance of rating microservice
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class RatingControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Ignore("It requires all services working - config, discovery and at least one instance of rating microservice")
    public void shouldGetRatingsForRoom() throws Exception {
        //given
        String expectedRatings = createExpectedRatingsJSON(createExpectedRatings());
        //when && then
        mockMvc.perform(get("/ratings"))
                .andExpect(jsonPath("$.[0].id", is(1)))
                .andExpect(jsonPath("$.[0].rate", is(5)))
                .andExpect(jsonPath("$.[0].description", is("Great room")))
                .andExpect(jsonPath("$.[0].roomId", is(1)))
                .andExpect(jsonPath("$.[0].runningServiceNumber", notNullValue()));
    }

    /*
    [{"id":1,"rate":5,"description":"Great room","roomId":1,"runningServiceNumber":1},{"id":2,"rate":4,"description":"Nice room but air conditioning doesnt work","roomId":1,"runningServiceNumber":1},{"id":3,"rate":1,"description":"Messy, hot room, we had bed bugs","roomId":2,"runningServiceNumber":1},{"id":4,"rate":2,"description":"Very bad experience, we found dead body in the closet","roomId":2,"runningServiceNumber":1},{"id":5,"rate":4,"description":"Nice room with nice view, could be bigger","roomId":3,"runningServiceNumber":1},{"id":6,"rate":3,"description":"It was not bad but toilet didnt flush","roomId":3,"runningServiceNumber":1}]
     */

}
