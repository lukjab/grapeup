package com.grape.hotelreservation.controller;

import com.grape.hotelreservation.dto.ReservationDTO;
import com.grape.hotelreservation.entity.Reservation;
import com.grape.hotelreservation.exceptions.NotFoundException;
import com.grape.hotelreservation.service.reservation.ReservationService;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import static com.grape.hotelreservation.constants.DateFormat.DATE_FORMATTER_DD_MM_YYYY;
import static com.grape.hotelreservation.constants.ErrorMessages.RESERVATION_NOT_FOUND_FOR_ID;
import static com.grape.hotelreservation.utils.TestData.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Test class for {@link ReservationController}
 */
public class ReservationControllerTest {

    private ReservationController reservationController;

    @Mock
    private ReservationService reservationService;

    @Mock
    private ModelMapper modelMapper;

    @Before
    public void init() {
        initMocks(this);
        reservationController = new ReservationController(modelMapper, reservationService, RESERVATION_SERVICE_URL);
    }

    @Test
    public void shouldListAllExistingReservations() {
        //given
        List<Reservation> dummyReservations = createEmptyReservations();
        when(reservationService.getAllReservations()).thenReturn(dummyReservations);
        when(modelMapper.map(any(), any())).thenReturn(DUMMY_RESERVATION_DTO);
        //when
        List<ReservationDTO> result = reservationController.getAllReservations();
        //then
        assertEquals(dummyReservations.size(), result.size());
        verify(reservationService, times(1)).getAllReservations();
    }

    @Test
    public void shouldGetReservationById() throws IOException, ParseException {
        //given
        Reservation dummyReservation = createDummyReservation();
        when(reservationService.getReservationById(DUMMY_RESERVATION_ID)).thenReturn(dummyReservation);
        when(modelMapper.map(dummyReservation, ReservationDTO.class)).thenReturn(DUMMY_RESERVATION_DTO);
        //when
        ReservationDTO result = reservationController.getReservationById(DUMMY_RESERVATION_ID);
        //then
        assertEquals(DUMMY_RESERVATION_USER_NAME, result.getUserName());
        assertEquals(DUMMY_RESERVATION_NUMBER_OF_PEOPLE, result.getNumberOfPeople());
        assertEquals(DUMMY_RESERVATION_START_DATE, result.getStartDate());
        assertEquals(DUMMY_RESERVATION_END_DATE, result.getEndDate());
        assertEquals(DUMMY_RESERVATION_ROOM_ID, result.getRoomId());
        verify(reservationService, times(1)).getReservationById(DUMMY_RESERVATION_ID);
    }

    @Test
    public void shouldThrowExceptionIfReservationNotFoundById() {
        //given
        when(reservationService.getReservationById(any())).thenThrow(new NotFoundException(RESERVATION_NOT_FOUND_FOR_ID));
        try {
            //when
            reservationController.getReservationById(NON_EXISTING_RESERVATION_ID);
            fail();
        } catch (NotFoundException ex) {
            //then
            assertEquals(ex.getMessage(), RESERVATION_NOT_FOUND_FOR_ID);
        }
        verify(reservationService, times(1)).getReservationById(NON_EXISTING_RESERVATION_ID);
    }

    @Test
    public void shouldCreateNewReservation() {
        //given
        Reservation createdReservation = createReservation(5L, RESERVATION_TO_CREATE.getUserName(), RESERVATION_TO_CREATE.getNumberOfPeople(), RESERVATION_TO_CREATE.getStartDate(), RESERVATION_TO_CREATE.getEndDate(), RESERVATION_TO_CREATE.getRoom().getId());
        when(reservationService.saveReservation(RESERVATION_TO_CREATE)).thenReturn(createdReservation);
        when(modelMapper.map(RESERVATION_TO_CREATE_DTO, Reservation.class)).thenReturn(RESERVATION_TO_CREATE);
        when(modelMapper.map(createdReservation, ReservationDTO.class)).thenReturn(RESERVATION_TO_CREATE_DTO);
        //when
        String result = reservationController.createReservation(RESERVATION_TO_CREATE_DTO);
        //then
        assertEquals(CREATED_RESERVATION_URL, result);
        verify(reservationService, times(1)).saveReservation(RESERVATION_TO_CREATE);
        verify(modelMapper, times(1)).map(RESERVATION_TO_CREATE_DTO, Reservation.class);
    }

    @Test
    public void shouldUpdateReservation() {
        //given
        Reservation reservationToUpdate = createReservation(1L, "userName", 6, LocalDate.parse(DUMMY_RESERVATION_START_DATE, DATE_FORMATTER_DD_MM_YYYY), LocalDate.parse(DUMMY_RESERVATION_END_DATE, DATE_FORMATTER_DD_MM_YYYY), DUMMY_RESERVATION_ROOM_ID);
        ReservationDTO reservationToUpdateDTO = createReservationDTO("userName", 6, DUMMY_RESERVATION_START_DATE, DUMMY_RESERVATION_END_DATE, DUMMY_RESERVATION_ROOM_ID);
        when(reservationService.saveReservation(any(Reservation.class))).thenReturn(reservationToUpdate);
        when(modelMapper.map(reservationToUpdateDTO, Reservation.class)).thenReturn(reservationToUpdate);
        when(modelMapper.map(reservationToUpdate, ReservationDTO.class)).thenReturn(reservationToUpdateDTO);
        //when
        reservationController.updateReservation(reservationToUpdateDTO, 1L);
        //then
        verify(reservationService, times(1)).saveReservation(any(Reservation.class));
        verify(modelMapper, times(1)).map(any(ReservationDTO.class), eq(Reservation.class));
        verify(modelMapper, times(1)).map(any(Reservation.class), eq(ReservationDTO.class));
    }

    @Test
    public void shouldDeleteReservation() {
        //given
        Long reservationToDeleteId = 1L;
        //when
        reservationController.deleteReservation(reservationToDeleteId);
        //then
        verify(reservationService, times(1)).deleteReservationById(reservationToDeleteId);
    }

    @Test
    public void shouldReturnReservationsForRoom() {
        //given
        Long roomId = 1L;
        List<Reservation> emptyReservations = createEmptyReservations();
        when(reservationService.getAllReservationsForRoom(roomId)).thenReturn(emptyReservations);
        when(modelMapper.map(any(Reservation.class), eq(ReservationDTO.class))).thenReturn(new ReservationDTO());
        //when
        List<ReservationDTO> result = reservationController.getAllReservationsForRoom(roomId);
        //then
        assertEquals(3, result.size());
        verify(reservationService, times(1)).getAllReservationsForRoom(roomId);
        verify(modelMapper, times(3)).map(any(Reservation.class), eq(ReservationDTO.class));
    }

}