package com.grape.hotelreservation.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.List;

import static org.assertj.core.util.Lists.newArrayList;

/**
 * Auxiliary class used for testing rating endpoint(code duplicate from rating service, but there is no time/need to define some general util/common service)
 */
public class RatingTestData {

    public static final String RATINGS_URL = "/ratings";
    public static final String RATINGS_FOR_ROOM_1_URL = RATINGS_URL.concat("/1");

    public static String createExpectedRatingsJSON(List<Rating> ratings) {
        JSONArray array = new JSONArray();
        for (Rating rating : ratings) {
            array.add(createJsonObjectForRating(rating));
        }

        return array.toJSONString();
    }

    private static JSONObject createJsonObjectForRating(Rating rating) {
        JSONObject object = new JSONObject();

        object.put("id", rating.getId());
        object.put("rate", rating.getRate());
        object.put("description", rating.getDescription());
        object.put("roomId", rating.getRoomId());

        return object;
    }

    public static List<Rating> createExpectedRatings() {
        return newArrayList(
                new Rating(1L, 5, "Great room", 1L),
                new Rating(2L, 4, "Nice room but air conditioning doesnt work", 1L),
                new Rating(3L, 1, "Messy, hot room, we had bed bugs", 2L),
                new Rating(4L, 2, "Very bad experience, we found dead body in the closet", 2L),
                new Rating(5L, 4, "Nice room with nice view, could be bigger", 3L),
                new Rating(6L, 3, "It was not bad but toilet didnt flush", 3L)
        );
    }

    public static List<Rating> createExpectedRatingsForRoom() {
        return newArrayList(new Rating(1L, 5, "Great room", 1L), new Rating(2L, 4, "Nice room but air conditioning doesnt work", 1L));
    }
}
