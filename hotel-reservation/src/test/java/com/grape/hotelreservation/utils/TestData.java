package com.grape.hotelreservation.utils;

import com.grape.hotelreservation.constants.RoomType;
import com.grape.hotelreservation.dto.ReservationDTO;
import com.grape.hotelreservation.entity.Reservation;
import com.grape.hotelreservation.entity.Room;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static com.grape.hotelreservation.constants.DateFormat.DATE_FORMATTER_DD_MM_YYYY;

/**
 * Auxiliary class for creating test data
 */
public class TestData { //todo probably divide into separate test data classes(by test data category etc)

    public static final String RESERVATIONS_URL = "/reservations";
    public static final String RESERVATION_BY_ID_URL = "/reservations/1";
    public static final String RESERVATION_BY_NON_EXISTING_ID_URL = "/reservations/11";

    public static final String UPDATE_RESERVATION_URL = "/reservations/2";

    public static final String RESERVATIONS_FOR_ROOM_URL = "/reservations?roomNumber=1";

    public static final Long DUMMY_RESERVATION_ID = 1L;
    public static final ReservationDTO DUMMY_RESERVATION_DTO = createReservationDTO("user1", 4, "01-01-2020", "01-05-2020", 1L);
    public static final String DUMMY_RESERVATION_USER_NAME = "user1";
    public static final Integer DUMMY_RESERVATION_NUMBER_OF_PEOPLE = 4;
    public static final String DUMMY_RESERVATION_START_DATE = "01-01-2020";
    public static final String DUMMY_RESERVATION_END_DATE = "01-05-2020";
    public static final Long DUMMY_RESERVATION_ROOM_ID = 1L;
    public static final long NON_EXISTING_RESERVATION_ID = 11L;

    //CREATION
    public static final Reservation RESERVATION_TO_CREATE = createReservation(null, "userName1", 6, LocalDate.parse("01-02-2020", DATE_FORMATTER_DD_MM_YYYY), LocalDate.parse("02-02-2020", DATE_FORMATTER_DD_MM_YYYY), 5L);
    public static final ReservationDTO RESERVATION_TO_CREATE_DTO = createReservationDTO("userName1", 6, "01-02-2020", "02-02-2020", 5L);
    public static final String RESERVATION_SERVICE_URL = "http://localhost:8080/reservations/";
    public static final String CREATED_RESERVATION_URL = RESERVATION_SERVICE_URL.concat("5");

    public static List<Reservation> createEmptyReservations() {
        return newArrayList(new Reservation(), new Reservation(), new Reservation());
    }

    public static Reservation createDummyReservation() throws IOException, ParseException {
        return createDummyReservations().get(0);
    }

    public static List<Reservation> createDummyReservations() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        File file = getFileByName("reservations.json");
        try (FileReader reader = new FileReader(file)) {
            JSONArray jsonArray = (JSONArray) parser.parse(reader);

            return newArrayList(
                    mapJSONObjectToReservation((JSONObject) jsonArray.get(0)),
                    mapJSONObjectToReservation((JSONObject) jsonArray.get(1))
            );
        }
    }

    private static File getFileByName(String fileName) {
        return new File(
                TestData.class.getClassLoader().getResource(fileName).getFile()
        );
    }

    public static Reservation createReservation(Long id, String userName, Integer numberOfPeople, LocalDate startDate, LocalDate endDate, Long roomId) {
        Room room = Room.builder()
                .id(roomId)
                .roomType(RoomType.getCorrectRoomType(numberOfPeople))
                .build();

        return Reservation.builder()
                .id(id)
                .userName(userName)
                .numberOfPeople(numberOfPeople)
                .startDate(startDate)
                .endDate(endDate)
                .room(room)
                .build();
    }

    public static ReservationDTO createReservationDTO(String userName, Integer numberOfPeople, String startDate, String endDate, Long roomId) {
        return new ReservationDTO(userName, numberOfPeople, startDate, endDate, roomId);
    }

    public static String createReservationDTOJSON(String userName, Integer numberOfPeople, String startDate, String endDate, Long roomId) {
        final JSONObject json = new JSONObject();

        if (userName != null) {
            json.put("userName", userName);
        }
        if (numberOfPeople != null) {
            json.put("numberOfPeople", numberOfPeople);
        }
        if (startDate != null) {
            json.put("startDate", startDate);
        }
        if (endDate != null) {
            json.put("endDate", endDate);
        }
        if (roomId != null) {
            json.put("roomId", roomId);
        }

        return json.toJSONString();
    }

    public static String createReservationToCreateDTO() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        File file = getFileByName("createReservationDTO.json");
        try (FileReader reader = new FileReader(file)) {
            String reservationToCreate = ((JSONObject) parser.parse(reader)).toJSONString();

            return reservationToCreate;
        }
    }

    public static String createDummyReservationDTO() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        File file = getFileByName("reservationDTOs.json");
        try (FileReader reader = new FileReader(file)) {
            JSONArray jsonArray = (JSONArray) parser.parse(reader);
            String dummyReservation = ((JSONObject) jsonArray.get(0)).toJSONString();

            return dummyReservation;
        }
    }

    public static String getExpectedReservationsJSON() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        File file = getFileByName("reservationDTOs.json");
        try (FileReader reader = new FileReader(file)) {
            JSONArray jsonArray = (JSONArray) parser.parse(reader);
            String expectedReservations = jsonArray.toJSONString();

            return expectedReservations;
        }
    }

    public static String getExpectedReservationsForRoomJSON() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        File file = getFileByName("reservationDTOs.json");
        try (FileReader reader = new FileReader(file)) {
            JSONArray jsonArrayAllReservations = (JSONArray) parser.parse(reader);
            JSONArray jsonArrayForRoom = new JSONArray();

            jsonArrayForRoom.add(jsonArrayAllReservations.get(0));
            jsonArrayForRoom.add(jsonArrayAllReservations.get(3));

            return jsonArrayForRoom.toJSONString();
        }
    }

    private static Reservation mapJSONObjectToReservation(JSONObject jsonObject) {
        return Reservation.builder()
                .id((Long) jsonObject.get("id"))
                .userName((String) jsonObject.get("userName"))
                .numberOfPeople(((Long) jsonObject.get("numberOfPeople")).intValue())
                .room(Room.builder().id((Long) jsonObject.get("roomId")).build())
                .startDate(LocalDate.parse((String) jsonObject.get("startDate"), DATE_FORMATTER_DD_MM_YYYY))
                .endDate(LocalDate.parse((String) jsonObject.get("endDate"), DATE_FORMATTER_DD_MM_YYYY))
                .build();
    }

}
