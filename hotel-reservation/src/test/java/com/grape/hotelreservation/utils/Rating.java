package com.grape.hotelreservation.utils;

import lombok.Builder;
import lombok.Getter;

/**
 * Auxiliary DTO class used in Rating endpoints testing
 */
@Getter
@Builder
public class Rating {

    public Rating(Long id, Integer rate, String description, Long roomId) {
        this.id = id;
        this.rate = rate;
        this.description = description;
        this.roomId = roomId;
    }

    private Long id;
    private Integer rate;
    private String description;
    private Long roomId;
    //private Integer runningServiceNumber;
}
