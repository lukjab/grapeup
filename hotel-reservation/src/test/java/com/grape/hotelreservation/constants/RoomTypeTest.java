package com.grape.hotelreservation.constants;

import com.grape.hotelreservation.exceptions.TooManyPeopleException;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.grape.hotelreservation.constants.RoomType.*;
import static org.junit.Assert.assertEquals;

@RunWith(DataProviderRunner.class)
public class RoomTypeTest {

    private static final int NUMBER_OF_PEOPLE_EXCEEDING_MAX_LIMIT = 9;

    @DataProvider
    public static Object[][] provideUpToFourPeople() {
        return new Object[][]{
                {1},
                {2},
                {3},
                {4}
        };
    }

    @DataProvider
    public static Object[][] provideBetweenFiveAndSixPeople() {
        return new Object[][]{
                {5},
                {6}
        };
    }

    @DataProvider
    public static Object[][] provideBetweenSevenAndEightPeople() {
        return new Object[][]{
                {7},
                {8}
        };
    }

    @Test
    @UseDataProvider("provideUpToFourPeople")
    public void shouldGetBasicRoomForLessOrEqualToFourPeople(Integer numberOfPeople) throws TooManyPeopleException {
        //when
        RoomType result = RoomType.getCorrectRoomType(numberOfPeople);
        //then
        assertEquals(BASIC, result);
    }

    @Test
    @UseDataProvider("provideBetweenFiveAndSixPeople")
    public void shouldGetSuiteRoomForFiveOrSixPeople(Integer numberOfPeople) throws TooManyPeopleException {
        //when
        RoomType result = RoomType.getCorrectRoomType(numberOfPeople);
        //then
        assertEquals(SUITE, result);
    }

    @Test
    @UseDataProvider("provideBetweenSevenAndEightPeople")
    public void shouldGetPenthouseRoomForSevenOrEightPeople(Integer numberOfPeople) throws TooManyPeopleException {
        //when
        RoomType result = RoomType.getCorrectRoomType(numberOfPeople);
        //then
        assertEquals(PENTHOUSE, result);
    }

    @Test(expected = TooManyPeopleException.class)
    public void shouldThrowExceptionForNumberOfPeopleExceedingMaxLimit() {
        //when && then
        RoomType.getCorrectRoomType(NUMBER_OF_PEOPLE_EXCEEDING_MAX_LIMIT);
    }
}