#!/bin/bash

cd ..
echo "Building app using maven."

./mvnw clean package -DskipTests
echo "Running built app locally."
java -jar ./target/hotelreservation-0.0.1-SNAPSHOT.jar & disown

sleep 6s #dummy way of securing that java app will get completely started before newman tests are executed
echo "Running tests."
newman run postman/KURS_CONFLUENCE_GU.postman_collection.json

echo "Shutting down java app."
kill $(ps aux | grep 'java -jar ./target/hotelreservation-0.0.1-SNAPSHOT.jar' | awk '{print $2}')

exit 0
